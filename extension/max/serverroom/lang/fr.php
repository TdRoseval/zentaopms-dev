<?php
$lang->serverroom->common      = 'IDC';
$lang->serverroom->browse      = 'Liste IDC';
$lang->serverroom->create      = 'Ajout IDC';
$lang->serverroom->edit        = 'Editer';
$lang->serverroom->editAction  = 'Editer IDC';
$lang->serverroom->delete      = 'Suppr. IDC';
$lang->serverroom->view        = 'D�tail IDC';
$lang->serverroom->all         = 'Tout';
$lang->serverroom->byQuery     = 'Recherche';
$lang->serverroom->name        = 'Nom';
$lang->serverroom->city        = 'Ville';
$lang->serverroom->line        = 'Ligne';
$lang->serverroom->bandwidth   = 'Bande Passante';
$lang->serverroom->provider    = 'Provider';
$lang->serverroom->owner       = 'Manager';
$lang->serverroom->region      = 'Zone';
$lang->serverroom->createdBy   = 'Cr�� par';
$lang->serverroom->createdDate = 'Cr�� le';
$lang->serverroom->editedBy    = 'Edit� par';
$lang->serverroom->editedDate  = 'Edit� le';
$lang->serverroom->noneCity    = 'N/A';

$lang->serverroom->empty = 'No IDC';

$lang->serverroom->lineList['']        = '';
$lang->serverroom->lineList['telecom'] = 'Telecom';
$lang->serverroom->lineList['unicom']  = 'Unicom';
$lang->serverroom->lineList['mobile']  = 'Mobile';
$lang->serverroom->lineList['double']  = 'Double';
$lang->serverroom->lineList['bgp']     = 'BGP';

$lang->serverroom->confirmDelete = 'Voulez-vous supprimer cet IDC ?';

$lang->serverroom->providerList['']  = '';
$lang->serverroom->providerList['aliyun']  = 'Ali Cloud';
$lang->serverroom->providerList['qingyun'] = 'Qing Cloud';

$lang->serverroom->cityList['']   = '';
$lang->serverroom->cityList['beijing']   = 'P�kin';
$lang->serverroom->cityList['hangzhou']  = 'Hangzhou';
$lang->serverroom->cityList['guangdong'] = 'Guangdong';
