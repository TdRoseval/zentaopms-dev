<?php
$config->custom->canAdd['baseline']    = 'objectList';
$config->custom->canAdd['nc']          = 'typeList,severityList';
$config->custom->canAdd['issue']       = 'typeList,severityList,priList';
$config->custom->canAdd['risk']        = 'categoryList,sourceList';
$config->custom->canAdd['opportunity'] = 'sourceList,typeList';
